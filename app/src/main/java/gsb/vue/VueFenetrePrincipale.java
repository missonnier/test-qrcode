package gsb.vue;

import android.app.Activity;
import android.os.Bundle;
import android.widget.ImageButton;

import gsb.controleur.ControleurFenetrePrincipale;

public class VueFenetrePrincipale extends Activity
{
    private ControleurFenetrePrincipale monControleur;
    /*private TextView titre;    */

    private ImageButton boutonRechercheMedic;
    private ImageButton boutonRechercheLaboratoire;
    private ImageButton boutonRechercheMedicQRCode;

    
    public String getInputNomUtilisateur(){
        //return this.titre.getText().toString();
        return "";        
    }

    /** Constructeur de la vue */
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.afficheprincipal);
        this.monControleur = new ControleurFenetrePrincipale(this);

        this.boutonRechercheMedic = (ImageButton)findViewById(R.id.boutonRechercheMedicamentCIS);
        this.boutonRechercheMedic.setOnClickListener(this.monControleur);

        this.boutonRechercheLaboratoire = (ImageButton)findViewById(R.id.boutonRechercheLaboratoire);
        this.boutonRechercheLaboratoire.setOnClickListener(this.monControleur);

        this.boutonRechercheMedicQRCode = (ImageButton)findViewById(R.id.boutonRechercheMedicamentQrcode);
        this.boutonRechercheMedicQRCode.setOnClickListener(this.monControleur);
    }
}
