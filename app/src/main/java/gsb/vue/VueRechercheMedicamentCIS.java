package gsb.vue;

import android.app.Activity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;

import gsb.controleur.ControleurRechercheMedicamentCIS;

/**
 * Created by agarcia on 25/04/2016.
 */
public class VueRechercheMedicamentCIS extends Activity {

    private ControleurRechercheMedicamentCIS monControleur;

    private ImageButton boutonRechercheMedicament;
    private EditText etRechercheCIS;

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.recherchemedicamentparcis);

        this.monControleur = new ControleurRechercheMedicamentCIS(this);

        //on récupère le objets dont on aura besoin dans le layout
        this.boutonRechercheMedicament = (ImageButton)findViewById(R.id.boutonRechercheMedicamentCIS);
        this.boutonRechercheMedicament.setOnClickListener(this.monControleur);

        this.etRechercheCIS = (EditText)findViewById(R.id.inputSaisieCIS);
    }

    public String getCodeCis(){
        return this.etRechercheCIS.getText().toString();
    }
}
