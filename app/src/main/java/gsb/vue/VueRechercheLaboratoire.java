package gsb.vue;

import android.app.Activity;
import android.os.Bundle;
import android.widget.Button;

import gsb.controleur.ControleurRechercheLaboratoire;

/**
 * Created by agarcia on 25/04/2016.
 */
public class VueRechercheLaboratoire extends Activity {

    private ControleurRechercheLaboratoire monControleur;
    private Button boutonAffichePraticien;
    private Button ajoutPraticien;
    private Button boutonSuppPrat;

    /** Constructeur de la vue */
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.recherchemedicamentparcis);


        this.monControleur = new ControleurRechercheLaboratoire(this);
/*

        //on récupère le bouton dans le layout AFFICHER
        this.boutonAffichePraticien = (Button)findViewById(R.id.boutonAffichePraticien);
        this.boutonAffichePraticien.setOnClickListener(this.monControleur);
*/

    }

}
