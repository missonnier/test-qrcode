package gsb.vue;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.TextView;

import gsb.controleur.ControleurRechercheMedicamentQRCode;

/**
 * Created by Fabrice Missonnier on 21/03/2018.
 */
public class VueRechercheMedicamentQRCode extends Activity {

    private ControleurRechercheMedicamentQRCode monControleur;
    private Button boutonAffichePraticien;
    private TextView tv;

    /**
     * Constructeur de la vue
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.recherchemedicamentqrcode);
        this.monControleur = new ControleurRechercheMedicamentQRCode(this);
        Button b = (Button) findViewById(R.id.buttonQRCode);
        tv = (TextView) findViewById(R.id.tvLeMedicament);

        b.setOnClickListener(this.monControleur);
    }


    /*fait le lien entre ce que le scan de qrcode a reçu et notre application*/
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {

        if (requestCode == 0) {
            if (resultCode == RESULT_OK) {
                String contents = intent.getStringExtra("SCAN_RESULT"); // This will contain your scan result
                String format = intent.getStringExtra("SCAN_RESULT_FORMAT");
                Log.e("hop", contents);
                tv.setText(contents);
            }
        }
    }

    public void erreurAppliNonInstallee() {
        this.tv.setText("Application non installée, télécharger com.google.zxing.client.android.SCAN");
    }
}
