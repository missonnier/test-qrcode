package gsb.vue;

import android.app.Activity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import gsb.controleur.ControleurAffichePraticien;
import gsb.controleur.ControleurVueMaps;

public class VueMaps extends Activity {

    private ControleurVueMaps monControleur;

    /**
     * Constructeur de la vue
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.affichepraticien);

        this.monControleur = new ControleurVueMaps(this);

    }
}
