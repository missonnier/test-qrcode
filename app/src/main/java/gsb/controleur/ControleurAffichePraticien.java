package gsb.controleur;

import android.os.AsyncTask;
import android.view.View;

import gsb.vue.R;


/**
 *
 * @author Valentin Bancarel
 */
public class ControleurAffichePraticien {//implements View.OnClickListener {

}
    //private VueAffichePraticien maVue;
    //private PraticienREST monModele;

    //lien sur la classe permettant de programmer le thread sur le modèle


    /*public ControleurAffichePraticien(VueAffichePraticien maVue) {
        this.maVue = maVue;
        //this.monModele = new PraticienREST();
        //on crée une nouvelle instance de la classe privée programmée ci-dessous

    }

    public void onClick(View view) {
        // On récupère l'identifiant du bouton
        switch(view.getId()) {
            // Si l'identifiant de la vue est celui du bouton le temps
            case R.id.boutonAffichePraticien:
                maVue.fermerClavier();
                if (maVue.testFormulaire()=="ok") {
                    try {
                        ThreadAccessModelePraticien tam;
                        //tam = new ThreadAccessModelePraticien(this.maVue, this.monModele);
                        //tam.execute(this.maVue.getInputSaisie());
                    } catch (Exception ex) {
                        // on remonte l'exception à la vue
                        this.maVue.setTextRetour("ERREUR - " + ex.toString());
                    }
                }
                break;

        }
    }
}


class ThreadAccessModelePraticien extends AsyncTask<String, Integer, Void>{
    private VueAffichePraticien maVue;
    //private PraticienREST monModele;
    private boolean statut;
    private String message;
    private String prat;
    String numpra;

    public ThreadAccessModelePraticien(VueAffichePraticien maVue, PraticienREST monModele){
        this.maVue = maVue;
        this.monModele = monModele;
    }
    //cette méthode est appelée dans le contrôleur (tam.execute)
    //elle permet d'accéder au modèle. C'est un thread différent de celui du
    //contrôleur. On a pas le droit ici d'accéder à la vue, uniquement au modèle.
    @Override
    protected Void doInBackground(String... params) {

        numpra = params[0];

        message = params[0];

        return null;
    }

    //cette méthode permet d'accéder à la vue
    //elle est appelée à la fin du thread et appartient au même thread que
    //celui du contrôleur. On a pas le droit ici d'accéder au modèle, uniquement
    //à la vue
    @Override
    protected void onPostExecute(Void result) {

     for (Praticien p : monModele.getLesPraticiens()){
            if(p.getPra_num() == Integer.decode(numpra)){
                monModele.getUnPraticien(numpra);
                this.statut = true;
                if (statut == true){
                    break;
                }
            }
            else {
                this.statut = false;
            }
        }
        if (statut == true){
            Praticien unPraticien = this.monModele.getUnPraticien(numpra);
            this.prat = unPraticien.toString();
            this.maVue.setTextRetour(prat);

        }else{
            Toast.makeText(this.maVue, "Le Praticien " + message + " n'existe pas", Toast.LENGTH_LONG).show();
        }
    }
}
*/