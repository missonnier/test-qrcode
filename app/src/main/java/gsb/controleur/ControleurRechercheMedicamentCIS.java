package gsb.controleur;

import android.content.Intent;
import android.view.View;

import gsb.modele.accesbdd.DAOMedicament;
import gsb.modele.metier.Medicament;
import gsb.vue.R;

import gsb.vue.VueRechercheMedicamentCIS;

/**
 * Created by Fabrice Missonnier on 29/03/2018.
 */

public class ControleurRechercheMedicamentCIS implements View.OnClickListener {
    private VueRechercheMedicamentCIS maVue;
    private DAOMedicament monModele;
    private Intent intent;

    public ControleurRechercheMedicamentCIS(VueRechercheMedicamentCIS maVue) {
        this.maVue = maVue;
        this.monModele = new DAOMedicament(this.maVue.getBaseContext());
    }

    @Override
    public void onClick(View view) {
        // On récupère l'identifiant du bouton
        switch(view.getId()) {

            case R.id.boutonRechercheMedicamentCIS:
                String CIS = this.maVue.getCodeCis();
                Medicament m = this.monModele.getMedicament(CIS);
                //this.maVue.affiche
                break;

        }
    }
}
