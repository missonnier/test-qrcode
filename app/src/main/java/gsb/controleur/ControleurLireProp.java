package gsb.controleur;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

/**
 * Created by tommy on 02/05/2016.
 */
public class ControleurLireProp extends Activity {
    public static final String PREFS_NAME = "configURLhttp";

    public  ControleurLireProp(){
/*
        // Restore preferences
        SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
        boolean silent = settings.getBoolean("silentMode", false);
        setSilent(silent);*/

        SharedPreferences pref = this.getSharedPreferences("URL", 0);
        SharedPreferences.Editor editor = pref.edit();
        editor.putString("VALUE", "http://test");
        editor.commit();

    }

    public void test() {
        Log.e("PROP", "Test");
        SharedPreferences prfs = getSharedPreferences("URL", Context.MODE_PRIVATE);
        String v = prfs.getString("VALUE", "");
        Log.e("PROP", v);
    }

}