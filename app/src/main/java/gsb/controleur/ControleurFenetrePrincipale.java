package gsb.controleur;

import android.content.Intent;
import android.view.View;

import gsb.vue.VueRechercheLaboratoire;
import gsb.vue.VueRechercheMedicamentCIS;
import gsb.vue.VueFenetrePrincipale;
import gsb.vue.R;
import gsb.vue.VueRechercheMedicamentQRCode;

import java.util.Timer;

/**
 *
 * @author fabrice
 */
public class ControleurFenetrePrincipale implements View.OnClickListener  {

    private VueFenetrePrincipale maVue;
    //private ModeleSOAP monModele;
    private Timer timer;
    final String UTILISATEUR = "utilisateur";

    public ControleurFenetrePrincipale(VueFenetrePrincipale maVue) {
        this.maVue = maVue;
       // this.monModele = new ModeleSOAP();

    }

    @Override
    public void onClick(View view) {
        Intent intent;

        // On récupère l'identifiant du bouton
        switch (view.getId()) {
            case R.id.boutonRechercheMedicamentCIS:
                intent = new Intent(maVue.getBaseContext(), VueRechercheMedicamentCIS.class);
                maVue.startActivity(intent);
                break;

            case R.id.boutonRechercheLaboratoire:
                intent = new Intent(maVue.getBaseContext(), VueRechercheLaboratoire.class);
                maVue.startActivity(intent);
                break;

            case R.id.boutonRechercheMedicamentQrcode:
                intent = new Intent(maVue.getBaseContext(), VueRechercheMedicamentQRCode.class);
                maVue.startActivity(intent);
                break;

        }
    }
}
