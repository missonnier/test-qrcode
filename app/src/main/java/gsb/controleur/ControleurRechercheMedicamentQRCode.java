package gsb.controleur;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.view.View;

import gsb.modele.qrcode.LireQRCode;
import gsb.vue.R;

import gsb.vue.VueRechercheMedicamentQRCode;

/**
 * Created by Fabrice Missonnier on 21/03/2018.
 */

public class ControleurRechercheMedicamentQRCode implements View.OnClickListener {
    private VueRechercheMedicamentQRCode maVue;
    private LireQRCode monModele;
    private Intent intent;

    public ControleurRechercheMedicamentQRCode(VueRechercheMedicamentQRCode maVue) {
        this.maVue = maVue;
        this.monModele = new LireQRCode();
    }

    @Override
    public void onClick(View view) {
        // On récupère l'identifiant du bouton
        switch(view.getId()) {

            case R.id.buttonQRCode:
                try{
                    Intent intent = new Intent(
                            "com.google.zxing.client.android.SCAN");
                    intent.putExtra("SCAN_MODE", "CODE_39,CODE_93,CODE_128,DATA_MATRIX,ITF,CODABAR,EAN_13,EAN_8,UPC_A,QR_CODE");

                    maVue.startActivityForResult(intent, 0);
                }
                catch (ActivityNotFoundException e){
                    this.maVue.erreurAppliNonInstallee();
                }
                break;
        }
    }

}
