package gsb.modele.accesbdd;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;


/**
 * Created by Fabrice Missonnier on 27/02/2018.
 */

public class MySQLiteHelper extends SQLiteOpenHelper {
    public static final String TABLE_MEDICAMENT = "Medicament";
    public static final String COLUMN_CODE_CIS = "codeCIS";
    public static final String COLUMN_DENOMINATION = "denomination";
    public static final String COLUMN_FORMEPHARMA = "formePharma";
    public static final String COLUMN_VOIEADMIN = "voieAdmin";
    public static final String COLUMN_STATUTADMINAMM = "statutAdminAMM";
    public static final String COLUMN_TYPEPROCAMM = "typeProcAMM";
    public static final String COLUMN_ETATCOMMERCIALISATION = "etatCommercialisation";
    public static final String COLUMN_DATEAMM = "dateAMM";
    public static final String COLUMN_STATUSBDM = "statusBDM";
    public static final String COLUMN_NUMAUTEUROP = "numAutEurop";
    public static final String COLUMN_TITULAIRE = "titulaire";
    public static final String COLUMN_SURVEILLANCE = "surveillance";
    public static final String COLUMN_CONDITIONPRESCR = "conditionPrescription";


    private static final String DATABASE_NAME = "medicaments.db";
    private static final int DATABASE_VERSION = 1;


    // Commande sql pour la création de la base de données
    private static final String DATABASE_CREATE = "CREATE TABLE Medicament (\n" +
            "codeCIS TEXT,\n" +
            "denomination TEXT,\n" +
            "formePharma TEXT,\n" +
            "voieAdmin TEXT,\n" +
            "statutAdminAMM TEXT,\n" +
            "typeProcAMM TEXT,\n" +
            "etatCommercialisation TEXT,\n" +
            "dateAMM TEXT,\n" +
            "statusBDM TEXT,\n" +
            "numAutEurop TEXT,\n" +
            "titulaire TEXT,\n" +
            "surveillance TEXT,\n" +
            "conditionPrescription TEXT,\n" +
            "PRIMARY KEY (codeCIS));";

    public MySQLiteHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase database) {
        database.execSQL(DATABASE_CREATE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.w(MySQLiteHelper.class.getName(),
                "Upgrading database from version " + oldVersion + " to "
                        + newVersion + ", which will destroy all old data");
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_MEDICAMENT);
        onCreate(db);
    }
}
