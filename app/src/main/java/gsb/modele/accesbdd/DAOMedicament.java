package gsb.modele.accesbdd;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import gsb.modele.metier.Medicament;

/**
 * Created by Fabrice Missonnier on 27/02/2018.
 */


public class DAOMedicament {

    private SQLiteDatabase database;
    private MySQLiteHelper dbHelper;

    private String[] allColumns = {
            MySQLiteHelper.COLUMN_CODE_CIS,
            MySQLiteHelper.COLUMN_DENOMINATION,
            MySQLiteHelper.COLUMN_FORMEPHARMA,
            MySQLiteHelper.COLUMN_VOIEADMIN,
            MySQLiteHelper.COLUMN_STATUTADMINAMM,
            MySQLiteHelper.COLUMN_TYPEPROCAMM,
            MySQLiteHelper.COLUMN_ETATCOMMERCIALISATION,
            MySQLiteHelper.COLUMN_DATEAMM,
            MySQLiteHelper.COLUMN_STATUSBDM,
            MySQLiteHelper.COLUMN_NUMAUTEUROP,
            MySQLiteHelper.COLUMN_TITULAIRE,
            MySQLiteHelper.COLUMN_SURVEILLANCE,
            MySQLiteHelper.COLUMN_CONDITIONPRESCR,
    };

    public DAOMedicament(Context context) {
        this.dbHelper = new MySQLiteHelper(context);
        this.open();
    }

    private void open() throws SQLException {
        this.database = dbHelper.getWritableDatabase();
    }

    public void close() {
        dbHelper.close();
    }

    /*public long createMedecin (Medecin leMedecin) {
        ContentValues values = new ContentValues();
        values.put(MySQLiteHelper.COLUMN_CODE_FINESS, leMedecin.getCodeFINESS());
        values.put(MySQLiteHelper.COLUMN_NOM, leMedecin.getNom());
        values.put(MySQLiteHelper.COLUMN_PRENOM, leMedecin.getPrenom());
        values.put(MySQLiteHelper.COLUMN_DEPARTEMENT, leMedecin.getDepartement());
        values.put(MySQLiteHelper.COLUMN_LIBEL_SPE, leMedecin.getLibelleSpecialite());
        values.put(MySQLiteHelper.COLUMN_DATE_ACC, leMedecin.getDateAccreditation().toString());
        values.put(MySQLiteHelper.COLUMN_STATUT_EX, leMedecin.getStatutExercice());

        //Si c'est une génération automatique

        //long insertId = database.insert(MySQLiteHelper.TABLE_MEDECIN, null, values);
        //Cursor cursor = database.query(MySQLiteHelper.TABLE_MEDECIN,
        //        allColumns, MySQLiteHelper.COLUMN_CODE_FINESS + " = " + insertId, null,
        //        null, null, null);

        return database.insert(MySQLiteHelper.TABLE_MEDECIN, null, values);
    }

    public int updateMedecin(String codeFiness, Medecin leMedecin){
        //La mise à jour d'un dans la BDD fonctionne plus ou moins comme une insertion
        //il faut simplement préciser quel medecin on doit mettre à jour grâce à la clé primaire
        ContentValues values = new ContentValues();
        values.put(MySQLiteHelper.COLUMN_CODE_FINESS, leMedecin.getCodeFINESS());
        values.put(MySQLiteHelper.COLUMN_NOM, leMedecin.getNom());
        values.put(MySQLiteHelper.COLUMN_PRENOM, leMedecin.getPrenom());
        values.put(MySQLiteHelper.COLUMN_DEPARTEMENT, leMedecin.getDepartement());
        values.put(MySQLiteHelper.COLUMN_LIBEL_SPE, leMedecin.getLibelleSpecialite());
        values.put(MySQLiteHelper.COLUMN_DATE_ACC, leMedecin.getDateAccreditation().toString());
        values.put(MySQLiteHelper.COLUMN_STATUT_EX, leMedecin.getStatutExercice());

        return this.database.update(MySQLiteHelper.TABLE_MEDECIN, values, MySQLiteHelper.COLUMN_CODE_FINESS + " = " + leMedecin.getCodeFINESS(), null);
    }

    public int removeMedecinWithCodeFiness(String codeFiness){
        //Suppression d'un medecin de la BDD grâce à l'ID
        return this.database.delete(MySQLiteHelper.TABLE_MEDECIN, MySQLiteHelper.COLUMN_CODE_FINESS + " = " + codeFiness, null);
    }*/

    /** Permet de convertir un cursor en médicament
     *
     * @param c
     * @return Medicament
     */
    private Medicament cursorToMedicament(Cursor c){
        //On créé un medecin vide
        Medicament m = new Medicament();
        //on lui affecte toutes les infos grâce aux infos contenues dans le curseur
        m.setCodeCIS(c.getString(0));
        m.setFormePharma(c.getString(1));
        m.setVoieAdmin(c.getString(2));
        m.setStatutAdminAMM(c.getString(3));
        m.setTypeProcAMM(c.getString(4));

        m.setEtatCommercialisation(c.getString(5));
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        try {
            Date d = sdf.parse(c.getString(6));
            m.setDateAMM(d);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        m.setStatusBDM(c.getString(7));
        m.setNumAutEurop(c.getString(8));
        m.setTitulaire(c.getString(9));

        switch (c.getString(9)) {
            case "true" : m.setSurveillance(true);
                break;
            case "false" : m.setSurveillance(false);
                break;
        }

        m.setConditionPrescription(c.getString(11));

        return m;
    }

    /**
     *  Récupère dans un cursor les valeurs correspondant à un médicament contenu dans la BDD
     *  (ici on sélectionne le médicament grâce à son code CIS)
     * @param codeCIS
     * @return Medicament
     */
    public Medicament getMedicament(String codeCIS){

        Cursor c = this.database.query(MySQLiteHelper.TABLE_MEDICAMENT, new String[] {
                                                MySQLiteHelper.COLUMN_CODE_CIS,
                                                MySQLiteHelper.COLUMN_DENOMINATION,
                                                MySQLiteHelper.COLUMN_FORMEPHARMA,
                                                MySQLiteHelper.COLUMN_VOIEADMIN,
                                                MySQLiteHelper.COLUMN_STATUTADMINAMM,
                                                MySQLiteHelper.COLUMN_TYPEPROCAMM,
                                                MySQLiteHelper.COLUMN_ETATCOMMERCIALISATION,
                                                MySQLiteHelper.COLUMN_DATEAMM,
                                                MySQLiteHelper.COLUMN_STATUSBDM,
                                                MySQLiteHelper.COLUMN_NUMAUTEUROP,
                                                MySQLiteHelper.COLUMN_TITULAIRE,
                                                MySQLiteHelper.COLUMN_SURVEILLANCE,
                                                MySQLiteHelper.COLUMN_CONDITIONPRESCR
                                            },
                                            MySQLiteHelper.COLUMN_CODE_CIS+ " LIKE \"" + codeCIS +"\"",
                                            null, null, null, null);
        c.moveToFirst();

        if (c==null){
            return null;
        }else {
            return cursorToMedicament(c);
        }
    }

    /** Retourne l'esemble des médicaments de la base
     *
     * @return ArrayList<Medicament>
     */
    public ArrayList<Medicament> getAllMedicaments() {
        ArrayList<Medicament> lesMedecins = new ArrayList();

        Cursor c = database.query(MySQLiteHelper.TABLE_MEDICAMENT,
                allColumns, null, null, null, null, null);

        c.moveToFirst();
        while (!c.isAfterLast()) {
            Medicament m = cursorToMedicament(c);
            lesMedecins.add(m);
            c.moveToNext();
        }

        c.close();
        return lesMedecins;
    }

}