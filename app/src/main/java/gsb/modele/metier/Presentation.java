/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gsb.modele.metier;

import java.util.Date;

/**
 *
 * @author fabrice
 */
public class Presentation {
    
    
/* 
[0]60058008  [1]3781819  [2]plaquette(s) PVC PVDC de 30 comprimé(s)  [3]Présentation active  [4]Déclaration de commercialisation  [5]14/08/2007  [6]3400937818193  [7]oui  [8]65%  [9]20,85  [10]21,87  [11]1,02  
[0]60058008  [1]3781831  [2]plaquette(s) PVC PVDC de 90 comprimé(s)  [3]Présentation active  [4]Déclaration de commercialisation  [5]14/08/2007  [6]3400937818315  [7]oui  [8]65%  [9]58,88  [10]61,64  [11]2,76  
[0]60058008  [1]3820442  [2]56 plaquette(s) PVC PVDC de 1 comprimé(s)  [3]Présentation active  [4]Déclaration de commercialisation  [5]21/01/2009  [6]3400938204421  [7]oui  
-  Code CIS (-> pas mis ici, appartient à Medicament
-  Code CIP7 (Code Identifiant de Présentation à 7 chiffres) 
-  Libellé de la présentation 
-  Statut administratif de la présentation 
-  Etat de commercialisation de la présentation tel que déclaré par le titulaire de l'AMM 
-  Date de la déclaration de commercialisation (formatJJ/MM/AAAA) 
-  Code CIP13 (Code Identifiant de Présentation à 13 chiffres) 
-  Agrément aux collectivités ("oui", "non" ou « inconnu ») 
-  Taux de remboursement (avec un séparateur « ; » entre chaque valeur quand il y en a 
plusieurs) 
-  Prix du médicament en euro 
-  Texte présentant les indications ouvrant droit au remboursement par l’assurance 
maladie s’il y a plusieurs taux de remboursement pour la même présentation. */
    private String codeCIP7;
    private String libellePres;
    private String statutPres;
    private String etatCommercialisation;
    private Date dateCommercialisation;
    private String codeCIP13;
    private String agrementCollectivites;
    private String tauxRbt;
    private String prixEuro;
    private String texteRbt;

    public Presentation(String codeCIP7, String libellePres, String statutPres, String etatCommercialisation, 
            Date dateCommercialisation, String codeCIP13, String agrementCollectivites, String tauxRbt, 
            String prixEuro, String texteRbt) {
        this.codeCIP7 = codeCIP7;
        this.libellePres = libellePres;
        this.statutPres = statutPres;
        this.etatCommercialisation = etatCommercialisation;
        this.dateCommercialisation = dateCommercialisation;
        this.codeCIP13 = codeCIP13;
        this.agrementCollectivites = agrementCollectivites;
        this.tauxRbt = tauxRbt;
        this.prixEuro = prixEuro;
        this.texteRbt = texteRbt;
    }
    
    public Presentation(String codeCIP7, String libellePres, String statutPres, String etatCommercialisation, 
            Date dateCommercialisation, String codeCIP13, String agrementCollectivites) {
        this.codeCIP7 = codeCIP7;
        this.libellePres = libellePres;
        this.statutPres = statutPres;
        this.etatCommercialisation = etatCommercialisation;
        this.dateCommercialisation = dateCommercialisation;
        this.codeCIP13 = codeCIP13;
        this.agrementCollectivites = agrementCollectivites;
        this.tauxRbt = "";
        this.prixEuro = "";
        this.texteRbt = "";
    }

    /* il arrive que le médicament n'ait pas de texte à la fin */
    public Presentation(String codeCIP7, String libellePres, String statutPres, 
            String etatCommercialisation, Date dateCommercialisation, String codeCIP13, 
            String agrementCollectivites, String tauxRbt, String prixEuro) {
        this.codeCIP7 = codeCIP7;
        this.libellePres = libellePres;
        this.statutPres = statutPres;
        this.etatCommercialisation = etatCommercialisation;
        this.dateCommercialisation = dateCommercialisation;
        this.codeCIP13 = codeCIP13;
        this.agrementCollectivites = agrementCollectivites;
        this.tauxRbt = tauxRbt;
        this.prixEuro = prixEuro;
        this.texteRbt = "";
    }
    
    public String getCodeCIP7() {
        return codeCIP7;
    }

    public void setCodeCIP7(String codeCIP7) {
        this.codeCIP7 = codeCIP7;
    }

    public String getLibellePres() {
        return libellePres;
    }

    public void setLibellePres(String libellePres) {
        this.libellePres = libellePres;
    }

    public String getStatutPres() {
        return statutPres;
    }

    public void setStatutPres(String statutPres) {
        this.statutPres = statutPres;
    }

    public String getEtatCommercialisation() {
        return etatCommercialisation;
    }

    public void setEtatCommercialisation(String etatCommercialisation) {
        this.etatCommercialisation = etatCommercialisation;
    }

    public Date getDateCommercialisation() {
        return dateCommercialisation;
    }

    public void setDateCommercialisation(Date dateCommercialisation) {
        this.dateCommercialisation = dateCommercialisation;
    }

    public String getCodeCIP13() {
        return codeCIP13;
    }

    public void setCodeCIP13(String codeCIP13) {
        this.codeCIP13 = codeCIP13;
    }

    public String getAgrementCollectivites() {
        return agrementCollectivites;
    }

    public void setAgrementCollectivites(String agrementCollectivites) {
        this.agrementCollectivites = agrementCollectivites;
    }

    public String getTxRbt() {
        return tauxRbt;
    }

    public void setTxRbt(String tauxRbt) {
        this.tauxRbt = tauxRbt;
    }

    public String getPrixEuro() {
        return prixEuro;
    }

    public void setPrixEuro(String prixEuro) {
        this.prixEuro = prixEuro;
    }

    public void setTexteRbt(String texteRbt) {
        this.texteRbt = texteRbt;
    }

    @Override
    public String toString() {
        return "Presentation{" + "codeCIP7=" + codeCIP7 + ", libellePres=" + libellePres + ", statutPres=" + statutPres + ", etatCommercialisation=" + etatCommercialisation + ", dateCommercialisation=" + dateCommercialisation + ", codeCIP13=" + codeCIP13 + ", agrementCollectivites=" + agrementCollectivites + ", tauxRbt=" + tauxRbt + ", prixEuro=" + prixEuro + ", texteRbt=" + texteRbt + '}';
    }

    
    
}
