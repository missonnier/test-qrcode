package gsb.modele.metier;

/**
 *
 * @author Fabrice Missonnier
 */
public class Dosage {
    
    /*
-  Désignation de l'élément pharmaceutique 
-  Dosage de la substance 
-  Référence de ce dosage (exemple : "[pour] un comprimé") 
-  Nature du composant (principe actif : « SA » ou fraction thérapeutique : « ST ») 
-  Numéro permettant de lier, le cas échéant, substances actives et fractions 
thérapeutiques*/
    
    private String designElemPharma;
    private String dosage;
    private String reference;
    private String nature;
    private String numSAFT;

    public Dosage(String designElemPharma, String dosage, String reference, String nature, String numSAFT) {
        this.designElemPharma = designElemPharma;
        this.dosage = dosage;
        this.reference = reference;
        this.nature = nature;
        this.numSAFT = numSAFT;
    }

    public String getDesignElemPharma() {
        return designElemPharma;
    }

    public void setDesignElemPharma(String designElemPharma) {
        this.designElemPharma = designElemPharma;
    }

    public String getDosage() {
        return dosage;
    }

    public void setDosage(String dosage) {
        this.dosage = dosage;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public String getNature() {
        return nature;
    }

    public void setNature(String nature) {
        this.nature = nature;
    }

    public String getNumSAFT() {
        return numSAFT;
    }

    public void setNumSAFT(String numSAFT) {
        this.numSAFT = numSAFT;
    }

    @Override
    public String toString() {
        return "Dosage{" + "designElemPharma=" + designElemPharma + ", dosage=" + dosage + ", reference=" + reference + ", nature=" + nature + ", numSAFT=" + numSAFT + '}';
    }

}
