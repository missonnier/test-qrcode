package gsb.modele.metier;

public class Praticien {
     
    private String pra_nom, pra_prenom, pra_adresse, pra_ville, pra_numtel, typ_code;
    private int pra_num, pra_cp;
    private float pra_coefnotoriete;

    public Praticien(){

    }

    public Praticien(int pra_num, String pra_nom, String pra_prenom, String pra_adresse, int pra_cp, String pra_ville, float pra_coefnotoriete, String typ_code, String pra_numtel )
    {
        this.pra_num = pra_num;
        this.pra_nom = pra_nom;
        this.pra_prenom = pra_prenom;
        this.pra_adresse = pra_adresse;
        this.pra_cp = pra_cp;
        this.pra_ville = pra_ville;
        this.pra_coefnotoriete = pra_coefnotoriete;
        this.typ_code = typ_code;
        this.pra_numtel = pra_numtel;

    }

    public int getPra_num() {
        return pra_num;
    }

    public void setPra_num(int pra_num) {
        this.pra_num = pra_num;
    }

    public String getpra_nom() {
        return pra_nom;
    }

    public void Setpra_nom(String pra_nom) {
        this.pra_nom = pra_nom;
    }

    public String getpra_prenom() {
        return pra_prenom;
    }

    public void Setpra_prenom(String pra_prenom) {
        this.pra_prenom = pra_prenom;
    } 
    
    public String getpra_adresse() {
        return pra_adresse;
    }

    public void Setpra_adresse(String pra_adresse) {
        this.pra_adresse = pra_adresse;
    }

    public String getpra_ville() {
        return pra_ville;
    }

    public void Setpra_ville(String pra_ville) {
        this.pra_ville = pra_ville;
    }   
    
    public String getpra_numtel() {
        return pra_numtel;
    }

    public void Setpra_numtel(String pra_numtel) {
        this.pra_numtel = pra_numtel;
    }

    public String gettyp_code() {
        return typ_code;
    }

    public void Settyp_code(String typ_code) {
        this.typ_code = typ_code;
    }

    public int getpra_cp() {
        return pra_cp;
    }

    public void Setpra_cp(int pra_cp) {
        this.pra_cp = pra_cp;
    } 
    
    public float getpra_coefnotoriete() {
        return pra_coefnotoriete;
    }

    public void Setpra_coefnotoriete(float pra_coefnotoriete) {
        this.pra_coefnotoriete = pra_coefnotoriete;
    }

    public String toString() {
        String toReturn = "";
        toReturn+="Vous avez demandez a afficher le praticien possédant le numero "+this.pra_num+"\n";
        toReturn+="Il s'agit de Mr/Mme "+this.pra_nom.toUpperCase()+" "+this.pra_prenom.toLowerCase()+"\n";
        toReturn+="Ce Praticien habite "+this.pra_adresse+" "+this.pra_cp+" "+this.pra_ville+"\n";
        toReturn+="Son type de code "+this.typ_code+"\n";
        toReturn+="Son téléphone est le "+this.pra_numtel+"\n";
        toReturn+="Son Coefficient est de : "+this.pra_coefnotoriete+"\n";
        return toReturn;
    }
}
